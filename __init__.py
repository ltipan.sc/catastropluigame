# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Catastro2
                                 A QGIS plugin
 Catastro 2.0
                             -------------------
        begin                : 2018-11-23
        copyright            : (C) 2018 by Jorge Cabrera
        email                : jorge.cabrera@ame.gob.ec
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load Catastro2 class from file Catastro2.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .catastro_2 import Catastro2
    return Catastro2(iface)
