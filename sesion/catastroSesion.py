#from ..herramientas.sincroniza_tool import SincronizaTool


class SesionCatastro:

    def __init__(self):
        self.sesionActiva = False
        self.usuario = None
        self.toolbarAvailability = False
    def activarSesion(self):
        self.sesionActiva = True

    def desactivaSesion(self):
        self.sesionActiva = False

    def setUsuario(self, usuario):
        self.usuario = usuario

    def wipeSesion(self):
        self.sesionActiva = None
        self.usuario = None

    def setToolBarAvailability(self):
        if not self.sesionActiva:
            self.toolbarAvailability = False
        else:
            self.toolbarAvailability = True
