# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sincroniza_dialog.ui'
#
# Created: Wed Nov 28 15:46:37 2018
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Sincroniza(object):
    def setupUi(self, Sincroniza):
        Sincroniza.setObjectName(_fromUtf8("Sincroniza"))
        Sincroniza.resize(461, 124)
        Sincroniza.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.verticalLayout = QtGui.QVBoxLayout(Sincroniza)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Sincroniza)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 1, 1, 1, 1)
        self.label_2 = QtGui.QLabel(Sincroniza)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 2, 1, 1, 1)
        self.comboBox = QtGui.QComboBox(Sincroniza)
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.gridLayout.addWidget(self.comboBox, 1, 2, 1, 2)
        self.comboBox_2 = QtGui.QComboBox(Sincroniza)
        self.comboBox_2.setObjectName(_fromUtf8("comboBox_2"))
        self.gridLayout.addWidget(self.comboBox_2, 2, 2, 1, 2)
        self.verticalLayout.addLayout(self.gridLayout)
        self.buttonBox = QtGui.QDialogButtonBox(Sincroniza)
        self.buttonBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Sincroniza)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Sincroniza.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Sincroniza.reject)
        QtCore.QMetaObject.connectSlotsByName(Sincroniza)

    def retranslateUi(self, Sincroniza):
        Sincroniza.setWindowTitle(_translate("Sincroniza", "Dialog", None))
        self.label.setText(_translate("Sincroniza", "Capa a sincronizar", None))
        self.label_2.setText(_translate("Sincroniza", "Clave Catastral", None))

