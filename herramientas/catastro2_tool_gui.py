# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from catastro2_dialog import Ui_CatastroDialogBase
#from .catastro2_tool import Catastro2Tool
import os, sys


class Catastro2Gui(QDialog, Ui_CatastroDialogBase):
    def __init__(self, parent, flags):
        QDialog.__init__(self, parent, flags)
        self.setupUi(self)

    def initGui(self):
        self.isOpened = True
        self.guardarEditarButton.setEnabled(False)
        self.cancelarRechazarButton.setEnabled(False)
        self.terminarEdicionButton.setEnabled(False)
        self.buscarLine.setEnabled(False)
        self.buscarButton.setEnabled(False)
