# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'catastro2_dialog.ui'
#
# Created: Wed Feb 13 10:31:24 2019
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CatastroDialogBase(object):
    def setupUi(self, CatastroDialogBase):
        CatastroDialogBase.setObjectName(_fromUtf8("CatastroDialogBase"))
        CatastroDialogBase.resize(673, 468)
        self.iniciarSesionButton = QtGui.QPushButton(CatastroDialogBase)
        self.iniciarSesionButton.setGeometry(QtCore.QRect(390, 10, 231, 23))
        self.iniciarSesionButton.setObjectName(_fromUtf8("iniciarSesionButton"))
        self.userInputLine = QtGui.QLineEdit(CatastroDialogBase)
        self.userInputLine.setGeometry(QtCore.QRect(60, 10, 113, 20))
        self.userInputLine.setText(_fromUtf8(""))
        self.userInputLine.setObjectName(_fromUtf8("userInputLine"))
        self.userPwdLine = QtGui.QLineEdit(CatastroDialogBase)
        self.userPwdLine.setGeometry(QtCore.QRect(260, 10, 113, 20))
        self.userPwdLine.setInputMask(_fromUtf8(""))
        self.userPwdLine.setText(_fromUtf8(""))
        self.userPwdLine.setEchoMode(QtGui.QLineEdit.Password)
        self.userPwdLine.setObjectName(_fromUtf8("userPwdLine"))
        self.label_2 = QtGui.QLabel(CatastroDialogBase)
        self.label_2.setGeometry(QtCore.QRect(190, 10, 71, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label = QtGui.QLabel(CatastroDialogBase)
        self.label.setGeometry(QtCore.QRect(10, 10, 71, 20))
        self.label.setObjectName(_fromUtf8("label"))
        self.tableWidget = QtGui.QTableWidget(CatastroDialogBase)
        self.tableWidget.setEnabled(False)
        self.tableWidget.setGeometry(QtCore.QRect(10, 110, 651, 321))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setRowCount(0)
        self.label_3 = QtGui.QLabel(CatastroDialogBase)
        self.label_3.setGeometry(QtCore.QRect(380, 10, 1, 20))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.guardarEditarButton = QtGui.QPushButton(CatastroDialogBase)
        self.guardarEditarButton.setGeometry(QtCore.QRect(30, 440, 231, 23))
        self.guardarEditarButton.setObjectName(_fromUtf8("guardarEditarButton"))
        self.terminarEdicionButton = QtGui.QPushButton(CatastroDialogBase)
        self.terminarEdicionButton.setEnabled(True)
        self.terminarEdicionButton.setGeometry(QtCore.QRect(440, 440, 201, 23))
        self.terminarEdicionButton.setObjectName(_fromUtf8("terminarEdicionButton"))
        self.label_4 = QtGui.QLabel(CatastroDialogBase)
        self.label_4.setGeometry(QtCore.QRect(380, 16, 0, 10))
        self.label_4.setText(_fromUtf8(""))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(CatastroDialogBase)
        self.label_5.setGeometry(QtCore.QRect(10, 80, 361, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.buscarLine = QtGui.QLineEdit(CatastroDialogBase)
        self.buscarLine.setGeometry(QtCore.QRect(60, 50, 311, 20))
        self.buscarLine.setText(_fromUtf8(""))
        self.buscarLine.setObjectName(_fromUtf8("buscarLine"))
        self.buscarButton = QtGui.QPushButton(CatastroDialogBase)
        self.buscarButton.setGeometry(QtCore.QRect(390, 50, 231, 23))
        self.buscarButton.setObjectName(_fromUtf8("buscarButton"))
        self.cancelarRechazarButton = QtGui.QPushButton(CatastroDialogBase)
        self.cancelarRechazarButton.setGeometry(QtCore.QRect(270, 440, 161, 23))
        self.cancelarRechazarButton.setObjectName(_fromUtf8("cancelarRechazarButton"))

        self.retranslateUi(CatastroDialogBase)
        QtCore.QMetaObject.connectSlotsByName(CatastroDialogBase)

    def retranslateUi(self, CatastroDialogBase):
        CatastroDialogBase.setWindowTitle(_translate("CatastroDialogBase", "Catastro", None))
        self.iniciarSesionButton.setAccessibleName(_translate("CatastroDialogBase", "<html><head/><body><p>pushButton</p></body></html>", None))
        self.iniciarSesionButton.setText(_translate("CatastroDialogBase", "Iniciar sesion", None))
        self.label_2.setText(_translate("CatastroDialogBase", "Contraseña:", None))
        self.label.setText(_translate("CatastroDialogBase", "Usuario:", None))
        self.label_3.setText(_translate("CatastroDialogBase", "0", None))
        self.guardarEditarButton.setText(_translate("CatastroDialogBase", "Guardar y editar otro predio", None))
        self.terminarEdicionButton.setText(_translate("CatastroDialogBase", "Terminar edición", None))
        self.label_5.setText(_translate("CatastroDialogBase", "Orden de trabajo:", None))
        self.buscarButton.setAccessibleName(_translate("CatastroDialogBase", "<html><head/><body><p>pushButton</p></body></html>", None))
        self.buscarButton.setText(_translate("CatastroDialogBase", "Buscar Orden de trabajo", None))
        self.cancelarRechazarButton.setText(_translate("CatastroDialogBase", "Cancelar/Rechazar", None))

