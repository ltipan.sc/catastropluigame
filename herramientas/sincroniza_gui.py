# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from sincroniza_dialog import Ui_Sincroniza
import os, sys


class SincronizaGui(QDialog, Ui_Sincroniza):
    def __init__(self, parent, flags):
        QDialog.__init__(self, parent, flags)
        self.setupUi(self)

        self.method = "fixed"
        self.comboBox = self.comboBox
        self.comboBox_2 = self.comboBox_2
        self.okButton = self.buttonBox.button(QDialogButtonBox.Ok)
        self.okButton.clicked.connect(self.accept)

        self.cancelButton = self.buttonBox.button(QDialogButtonBox.Cancel)
        self.cancelButton.clicked.connect(self.close)

    def initGui(self):
        pass