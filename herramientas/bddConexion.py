# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
from qgis.core import QgsMessageLog
from ..utilitarios.configurations import configuracionGlobal


class BDDConexion:
    """
    Clase responsable de crear la conexion a la base de datos gráfica
    """
    def __init__(self):
        self.configuracionGlobal = configuracionGlobal()
        try:
            self.conexion = psycopg2.connect(self.configuracionGlobal.conexion)
            QgsMessageLog.logMessage(self.configuracionGlobal.conexion,'conexion')
            self.conexion_editor = psycopg2.connect(self.configuracionGlobal.conexion_editor)
            QgsMessageLog.logMessage(self.configuracionGlobal.conexion_editor, 'conexion')
        except(Exception, psycopg2.Error) as error:
            QgsMessageLog.logMessage("Error en la conexion" + error, 'Catastro AME')
        self.cursor = self.conexion.cursor()

    def close(self):
        self.conexion.close()
        self.conexion_editor.close()
