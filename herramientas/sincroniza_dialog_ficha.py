# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sincroniza_dialog_ficha.ui'
#
# Created: Tue Jan 22 14:18:10 2019
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FichaSincronizada(object):
    def setupUi(self, FichaSincronizada):
        FichaSincronizada.setObjectName(_fromUtf8("FichaSincronizada"))
        FichaSincronizada.setWindowModality(QtCore.Qt.WindowModal)
        FichaSincronizada.resize(515, 226)
        self.gridLayout = QtGui.QGridLayout(FichaSincronizada)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 5, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 2, 0, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem2, 5, 0, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 2, 1, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.claveCatastralLabel = QtGui.QLabel(FichaSincronizada)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.claveCatastralLabel.setFont(font)
        self.claveCatastralLabel.setObjectName(_fromUtf8("claveCatastralLabel"))
        self.verticalLayout.addWidget(self.claveCatastralLabel)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem4)
        self.nombrePropietarioLabel = QtGui.QLabel(FichaSincronizada)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nombrePropietarioLabel.setFont(font)
        self.nombrePropietarioLabel.setObjectName(_fromUtf8("nombrePropietarioLabel"))
        self.verticalLayout.addWidget(self.nombrePropietarioLabel)
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem5)
        self.areaLabel = QtGui.QLabel(FichaSincronizada)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.areaLabel.setFont(font)
        self.areaLabel.setObjectName(_fromUtf8("areaLabel"))
        self.verticalLayout.addWidget(self.areaLabel)
        self.gridLayout.addLayout(self.verticalLayout, 4, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.DataLayout = QtGui.QVBoxLayout()
        self.DataLayout.setObjectName(_fromUtf8("DataLayout"))
        self.claveCatastralData = QtGui.QLabel(FichaSincronizada)
        self.claveCatastralData.setObjectName(_fromUtf8("claveCatastralData"))
        self.DataLayout.addWidget(self.claveCatastralData)
        spacerItem6 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.DataLayout.addItem(spacerItem6)
        self.nombrePropietarioData = QtGui.QLabel(FichaSincronizada)
        self.nombrePropietarioData.setObjectName(_fromUtf8("nombrePropietarioData"))
        self.DataLayout.addWidget(self.nombrePropietarioData)
        spacerItem7 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.DataLayout.addItem(spacerItem7)
        self.areaGraficaData = QtGui.QLabel(FichaSincronizada)
        self.areaGraficaData.setObjectName(_fromUtf8("areaGraficaData"))
        self.DataLayout.addWidget(self.areaGraficaData)
        self.horizontalLayout.addLayout(self.DataLayout)
        self.gridLayout.addLayout(self.horizontalLayout, 4, 1, 1, 1)
        self.line = QtGui.QFrame(FichaSincronizada)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 3, 0, 1, 2)
        self.label = QtGui.QLabel(FichaSincronizada)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.label_2 = QtGui.QLabel(FichaSincronizada)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.retranslateUi(FichaSincronizada)
        QtCore.QMetaObject.connectSlotsByName(FichaSincronizada)

    def retranslateUi(self, FichaSincronizada):
        FichaSincronizada.setWindowTitle(_translate("FichaSincronizada", "Dialog", None))
        self.claveCatastralLabel.setText(_translate("FichaSincronizada", "Clave Catastral", None))
        self.nombrePropietarioLabel.setText(_translate("FichaSincronizada", "Nombre Propietario", None))
        self.areaLabel.setText(_translate("FichaSincronizada", "Area Gráfica", None))
        self.claveCatastralData.setText(_translate("FichaSincronizada", "ClaveCatastralData", None))
        self.nombrePropietarioData.setText(_translate("FichaSincronizada", "NombrePropietarioData", None))
        self.areaGraficaData.setText(_translate("FichaSincronizada", "AreaGraficaData", None))
        self.label.setText(_translate("FichaSincronizada", "La sincronización se ha realizado con los siguientes datos", None))
        self.label_2.setText(_translate("FichaSincronizada", "<html><head/><body><p><img src=\":/plugins/Catastro2/icons/logoPlataforma.png\"/></p></body></html>", None))

