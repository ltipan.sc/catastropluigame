# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.core import QgsMessageLog
from qgis.gui import *


class Catastro2:

    # Variable de clase global que almacena el usuario que esta actualmente en el sistema para usarlo en auditoria
    catastroUser = None

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface

    def getCapasCatastro(self):
        """
        Crea una lista de capas que pertenecen al catastro para su sincronizacion con la
        BDD de catastro usando el WS
        :return: Lista de Capas
        """
        layerList = []
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if layer.type() == QgsMapLayer.VectorLayer:
                typeGeom = QgsWKBTypes.displayString(int(QgsWKBTypes.flatType(int(layer.wkbType()))))
                if layer.name().startswith(
                        'm_edit_') and layer.type() == QgsMapLayer.VectorLayer and typeGeom == 'MultiPolygon':
                    layerList.append(layer)
        return layerList

    def getCapasCatastroReferencia(self):
        """
        Crea una lista de capas de referencia WFS que pertenecen al catastro en la TOC
        :rtype: List
        :return: Lista de Capas
        """
        layerList = []
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if layer.type() == QgsMapLayer.VectorLayer:
                typeGeom = QgsWKBTypes.displayString(int(QgsWKBTypes.flatType(int(layer.wkbType()))))
                if layer.name().endswith(
                        'referencia') and layer.type() == QgsMapLayer.VectorLayer and typeGeom == 'MultiPolygon':
                    layerList.append(layer)
        return layerList

    def setCatastroUser(self, user):
        Catastro2.catastroUser = user

    @staticmethod
    def getCatastroUser():
        if Catastro2.catastroUser is None:
            QgsMessageLog.logMessage('No hay usuario registrado', 'Catastro AME - Usuario')
        else:
            user = Catastro2.catastroUser
            return user