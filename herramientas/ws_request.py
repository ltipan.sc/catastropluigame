# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.core import QgsMessageLog
from qgis.gui import *
import requests
import xml.etree.cElementTree as ET
from sincroniza_gui import SincronizaGui
from sincroniza_dialog_ficha_gui import SincronizaFichaGui
from ..utilitarios.configurations import configuracionGlobal


class WsRequest:

    def __init__(self):
        # Save reference to the QGIS interface
        self.configuracionGlobal = configuracionGlobal()
        self.syncEndPoint = self.configuracionGlobal.sincronizacionEndPoint
        self.ruc = self.configuracionGlobal.ruc

    def mensaje(self, texto, tipo):
        msg = QMessageBox()
        msg.setIcon(tipo)
        msg.setText(texto)
        msg.setWindowTitle("Sistema de gestion catastral")
        retval = msg.exec_()

    def requestPredioSyncronization(self, claveCatastral):
        """
        Realiza el request al WS de sincronizacion de datos entre SIC y GIS AME
        :return: Diccionario que contiene los atributos recuperados del SIC
        preDominio = Tipo de dominio Publico -> 1, Privado -> 2
        nombrePredio= Nombre del Predio
        idRepLegal= Identificador del representante Legal
        nombrePropietarioAnt= Nombre del propietario Anterior
        idPropietario= Identificador del Propietario
        rucPropietario= RUC del Propietario
        preTipo= Ambito del predio Urbano -> 1, Rural -> 2
        preNotaria=
        preNumeroDivisiones
        prePropiedadHorizontal
        """
        ruc = self.ruc
        with requests.Session() as sesionSync:
            sesionSync.get(self.syncEndPoint)
            sesionSync.auth = ('ame2017', 'ame2017')
            xmlForRequest = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice/">
               <soapenv:Header/>
                   <soapenv:Body>
                      <web:consultarCampoGis>
                         <!--Optional:-->
                         <catastro>""" + claveCatastral + """</catastro>
                         <!--Optional:-->
                         <ruc>""" + ruc + """</ruc>
                      </web:consultarCampoGis>
                   </soapenv:Body>
            </soapenv:Envelope>"""
            headers = {'Content-Type': 'application/xml'}
            response = ET.fromstring(
                sesionSync.post(self.syncEndPoint, data=xmlForRequest, headers=headers).text.decode('utf-8'))
            # Crea diccionario para generar respuesta de la funcion
            responseDictionary = {}
            for child in response:
                for element in child:
                    for e in element:
                        for i in e:
                            responseDictionary[i.tag] = i.text
            if len(responseDictionary) < 2:
                self.mensaje(u'No se ha encontrado datos para sincronizar en el SIC \n '
                             u'Es necesario crear la ficha del predio', QMessageBox.Critical)
                return None
            return responseDictionary