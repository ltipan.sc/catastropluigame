# -*- coding: utf-8 -*-
import os
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.core import QgsMessageLog
from qgis.gui import *
from ..utilitarios.configurations import configuracionGlobal


class Referencia:

    def __init__(self, iface):
        self.iface = iface
        self.configuracionGlobal = configuracionGlobal()
        self.raizArbolDeCapas = QgsProject.instance().layerTreeRoot()
        self.referenceGroup = None
        self.referenceLayerPredios = None
        self.referenceLayerEdificaciones = None

    def addLayerGroup(self, groupName):
        """
        Agrega un grupo al arbol de capas
        :param groupName: String Nombre del grupo
        :return:
        """
        if not self.layerGroupExists(groupName):
            referenceGroup = self.raizArbolDeCapas.addGroup(groupName)
            self.referenceGroup = referenceGroup

    def addLayer(self, uri, nombre_capa, qml):
        """
        Agrega una capa vectorial, usando un estilo predeterminado
        :param uri: Uri para acceso al WFS
        :param nombre_capa: Nombre e la capa en el arbol de capas
        :param qml: Nombre del archivo de estilo generado en QGIS
        :return: Void
        """
        LayerObj = QgsVectorLayer(uri, nombre_capa, 'WFS')
        QgsMapLayerRegistry.instance().addMapLayer(LayerObj)
        self.styleLayer(LayerObj, qml)

    def styleLayer(self, layer, qml):
        """
        Pone un estilo determinado a una capa que ya existe en el arbol de capas
        :param layer: Layer del arbol de capas que se cambiará su estilo
        :param qml: Archivo qml de estilo creado en QGIS
        :return: Void
        """
        dirname, filename = os.path.split(os.path.abspath(__file__))
        layer.loadNamedStyle(dirname + os.sep + qml)
        layer.triggerRepaint()

    def layerGroupExists(self, groupName):
        """
        Verifica si un grupo de determinado nombre ya existe dentro del arbol de capas
        :param groupName: Nombre del grupo
        :return: Boolean flag
        """
        canvasLayers=[]
        raiz = QgsProject.instance().layerTreeRoot()
        flag = None
        for node in raiz.children():
            canvasLayers.append(node.name())
        for node in raiz.children():
            if str(node.name()) == groupName:
                flag = True
                return flag
            else:
                flag = False
        return flag

    def layerExists(self, nombre):
        """
        Verifica si una capa ya existe dentro del layer de capas
        :param nombre: Nombre de la capa en el arbol de capas
        :return: Boolean
        """
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if str(layer.name()) == nombre:
                return True

    def getWfsUri(self, tipo):
        """
        Metodo que genera el URI para invocar un servicio WFS
        :param tipo: Corresponde al tipo de capa predio, construccion
        :return:
        """
        geoserverIp = self.configuracionGlobal.geoserverIp
        geoserverPort = self.configuracionGlobal.geoserverPort
        srsName = self.configuracionGlobal.srsCapas
        geoserverWorkspace= self.configuracionGlobal.geoserverWorkspace
        uri = 'http://' + geoserverIp + ':' + geoserverPort + '/geoserver/wfs?srsname=' + srsName + '&typename=' \
              + geoserverWorkspace + ':' + tipo + '&version=1.0.0&request=GetFeature&service=WFS'
        return uri







