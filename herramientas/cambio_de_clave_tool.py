# -*- coding: utf-8 -*-
import os
from PyQt4.QtCore import *
from PyQt4.QtCore import *
from qgis.core import *
from qgis.core import QgsMessageLog
from qgis.gui import *
from ..utilitarios.configurations import configuracionGlobal
from clickedPoint import SendPointToolCoordinates
from catastro_2 import Catastro2
from cambio_clave_gui import CambioClaveGui, QAction, QIcon
from ws_request import WsRequest
from validaciones import Validaciones
from bddConexion import BDDConexion
from bdd_gis_update import BDDGisUpdate


class CambioClaveTool:
    def __init__(self, iface, toolbar):
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        # Clave catastral consultada en SIC
        self.claveCatastralConsulta = None
        # Clave catastral Nueva ingresada por el usuario
        self.claveCatastralNueva = None
        self.predioSelected = None
        self.diccionarioSincronizacion=None
        self.action_cambio_clave = QAction(QIcon(':/plugins/Catastro2/icons/cambio_clave_icon.png'),
                                           "Cambio de Clave",  self.iface.mainWindow())
        self.action_cambio_clave.setEnabled(True)
        toolbar.addAction(self.action_cambio_clave)
        self.action_cambio_clave.triggered.connect(self.showDialog)
        flags = Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMaximizeButtonHint
        self.cambioClaveWidget = CambioClaveGui(self.iface.mainWindow(), flags)
        self.cambioClaveWidget.seleccionPredioGrafico.setEnabled(False)
        self.cambioClaveWidget.deseleccionPredioGrafico.setEnabled(False)
        self.cambioClaveWidget.AceptarButton.setEnabled(False)
        self.cambioClaveWidget.BuscarPredio_pushButton.clicked.connect(lambda: self.buscarPredio())
        self.cambioClaveWidget.seleccionPredioGrafico.clicked.connect(lambda: self.mainSeleccion())
        self.cambioClaveWidget.deseleccionPredioGrafico.clicked.connect(lambda: self.deseleccionPredio())
        self.cambioClaveWidget.AceptarButton.clicked.connect(lambda: self.aceptarCambio())
        self.cambioClaveWidget.CancelarButton.clicked.connect(lambda: self.close())
        self.configuracionGlobal = configuracionGlobal()
        self.wsRequest = WsRequest()
        self.validaciones = Validaciones()
        self.bdd_conexion = BDDConexion()
        self.bdd_update = BDDGisUpdate(self.iface)
        self.catastro2 = Catastro2(self.iface)

    def showDialog(self):
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.cambioClaveWidget)

    def aceptarCambio(self):
        """
        Metodo usado para aceptar los cambios de claves catastrales, reliza las validaciones correspondientes
        a las claves catastrales ingresadas, si el predio es valido o si se ha duplicado
        :return: void
        """
        self.claveCatastralConsulta = self.cambioClaveWidget.originalClaveCatastral.text()
        self.claveCatastralNueva = self.cambioClaveWidget.inputNuevaClaveCatastral.text()
        isValid = self.validaciones.validaClaveCatastral(self.claveCatastralNueva)
        if isValid:
            self.cambioClaveWidget.label_3.setText(u'')
        else:
            self.cambioClaveWidget.label_3.setText('<font size="3" color="red">' + u'La clave ingresada no es válida \n '
                                                                                   u'se acepta solo numeros, la clave debe '
                                                                                   u'tener logitud de 19 caracteres' + '</font>')
            return -1

        isDuplicated = self.validaciones.validaClaveDuplicada(self.claveCatastralNueva)
        if isDuplicated:
            self.cambioClaveWidget.label_3.setText(u'')
        else:
            self.cambioClaveWidget.label_3.setText(
                '<font size="3" color="red">' + u'La clave ingresada esta duplicada \n '
                                                u'revise las claves en la cartografia predial' + '</font>')
            return -1

        if isValid and isDuplicated:
            bdd_schema = self.configuracionGlobal.dbSchema
            usuario = Catastro2.getCatastroUser()
            self.bdd_update.updateClaveCatastral(bdd_schema, self.claveCatastralConsulta, self.claveCatastralNueva)
            self.bdd_update.insertAuditoriaCambioClave(bdd_schema, self.claveCatastralConsulta, self.claveCatastralNueva, usuario)
            self.reloadLayer()
            self.limpiaFicha()
            self.limpiaVariables()
            self.cambioClaveWidget.label_3.setText('<b>' + u'La Clave se ha cambiado con éxito' + '</b>')
            self.iface.mapCanvas().refresh()
            self.deseleccionPredio()

    def close(self):
        self.cambioClaveWidget.close()

    def setClaveCatastralConsulta(self):
        self.claveCatastralConsulta = self.cambioClaveWidget.claveCatastralSIC_textBox.text()

    def setDiccionarioSincronizacion(self, claveCatastral):
        self.diccionarioSincronizacion = self.wsRequest.requestPredioSyncronization(claveCatastral)

    def buscarPredio(self):
        """
        Metodo que involucra a los demas metodos para realizar una busqueda de un predio en la base de datos
        alfanumérica del SIC.
        :return: Void
        """
        self.setClaveCatastralConsulta()
        self.setDiccionarioSincronizacion(self.claveCatastralConsulta)
        self.llenarFicha(self.diccionarioSincronizacion)
        self.cambioClaveWidget.AceptarButton.setEnabled(True)
        self.cambioClaveWidget.seleccionPredioGrafico.setEnabled(True)
        self.cambioClaveWidget.deseleccionPredioGrafico.setEnabled(True)

    def llenarFicha(self, diccionario):
        """
        Metodo usado para el llenado de la ficha en el elemento Dock de cambio de clave
        :param diccionario: Diccionario de datos recibido a traves de un web service de conexion entre
        SIC y GIS AME
        :return: Void
        """
        self.cambioClaveWidget.ClaveCatastralValue.setText(self.claveCatastralConsulta)
        self.cambioClaveWidget.IDPropietarioValue.setText(diccionario['idRepLegal'])
        self.cambioClaveWidget.NombrePropietarioValue.setText(diccionario['nombreCiudadano'])
        self.cambioClaveWidget.NombrePropietarioAnteriorValue.setText(diccionario['nombrePropietarioAnt'])
        self.cambioClaveWidget.NombrePredioValue.setText(diccionario['nombrePredio'])
        self.cambioClaveWidget.DominioValue.setText(diccionario['preDominio'])
        if diccionario['preTipo'] == '2':
            self.cambioClaveWidget.TipoValue.setText(u'Privado')
        elif diccionario['preTipo'] == '1':
            self.cambioClaveWidget.TipoValue.setText(u'Público')
        else:
            self.cambioClaveWidget.TipoValue.setText(u'---')
        self.cambioClaveWidget.NotariaValue.setText(diccionario['preNotaria'])

    def limpiaFicha(self):
        """
        Limia el contenido de la ficha contenida en el Dock de cambio de clave
        :return: Void
        """
        self.cambioClaveWidget.ClaveCatastralValue.setText('')
        self.cambioClaveWidget.IDPropietarioValue.setText('')
        self.cambioClaveWidget.NombrePropietarioValue.setText('')
        self.cambioClaveWidget.NombrePropietarioAnteriorValue.setText('')
        self.cambioClaveWidget.NombrePredioValue.setText('')
        self.cambioClaveWidget.DominioValue.setText('')
        self.cambioClaveWidget.TipoValue.setText('')
        self.cambioClaveWidget.NotariaValue.setText('')
        self.cambioClaveWidget.originalClaveCatastral.setText('')
        self.cambioClaveWidget.inputNuevaClaveCatastral.setText('')
        self.cambioClaveWidget.label_3.setText('')
        self.cambioClaveWidget.seleccionPredioGrafico.setEnabled(False)
        self.cambioClaveWidget.deseleccionPredioGrafico.setEnabled(False)
        self.cambioClaveWidget.AceptarButton.setEnabled(False)

    def limpiaVariables(self):
        """
        Metodo que hace el encerado de las variables para realizar nuevas busquedas
        :return: Void
        """
        self.claveCatastralNueva = None
        self.claveCatastralConsulta = None

    def setSelectedPredio(self, layer):
        self.predioSelected = layer.selectedFeatures()

    def reloadLayer(self):
        """
        Metodo encargado del redibujo de las capas wfs para mostrar la actualizadion de la clave catastral
        :return: Void
        """
        layers = self.catastro2.getCapasCatastroReferencia()
        for layer in layers:
            layer.reload()

    def seleccionPredio(self):
        """
        Realiza la seleccion del predio al que se cambiara la clave.
        Convierte en capa activa al WFS de predios referenciapara iniciar la seleccion
        mediante el uso del maptool SendPointToolCoordinates
        :return: Void
        """
        # Traigo las capas de referencia
        layers = self.catastro2.getCapasCatastroReferencia()
        # Vuelvo activa la capa de Predios referencia
        for layer in layers:
            if layer.name() == 'Predio referencia':
                self.iface.setActiveLayer(layer)
        # Se crean las varables para enviar a la subclase
        layer, canvas, widget = self.iface.activeLayer(), self.iface.mapCanvas(), self.cambioClaveWidget
        # Inyecto las variables en la subclase
        send_point_tool_coordinates = SendPointToolCoordinates(
            canvas,
            layer,
            widget
        )
        canvas.setMapTool(send_point_tool_coordinates)

    def deseleccionPredio(self):
        """
        Limpia la seleccion de todas las capas
        :return: Void
        """
        for a in self.iface.attributesToolBar().actions():
            if a.objectName() == 'mActionDeselectAll':
                a.trigger()
                break
        self.clearTabCatastroAME()

    def clearTabCatastroAME(self):
        self.cambioClaveWidget.originalClaveCatastral.clear()

    def mainSeleccion(self):
        self.seleccionPredio()



