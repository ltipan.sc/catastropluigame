# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
from sincroniza_gui import SincronizaGui
from sincroniza_dialog_ficha_gui import SincronizaFichaGui
from ws_request import WsRequest
from ..utilitarios.configurations import configuracionGlobal


class SincronizaTool:

    def __init__(self, iface, toolbar):
        self.iface = iface
        self.action_sincroniza = QAction(QIcon(':/plugins/Catastro2/icons/sync.png'),
                                         "Sincronizacion de geometrias", self.iface.mainWindow())

        self.action_sincroniza.setEnabled(True)
        toolbar.addAction(self.action_sincroniza)
        self.action_sincroniza.triggered.connect(self.showDialog)
        self.campoClavePredio = 'clave_catastral'
        self.campoClaveEdificacion = 'id'
        self.claveCatastral = ''
        self.configuracionGlobal = configuracionGlobal()
        self.diccionarioSincronizacion = None
        self.wsRequest = WsRequest()

    def FichaSincronizada(self,  dicionarioDeCampos):
        """
        Llena la ficha de sincronizacion de predios entre base gráfica y alfanumérica
        :param dicionarioDeCampos: Diccionario de datos recibido a traves del web service de consulta de
        predio en el SIC
        :return:
        """
        nombrePropietarioData = dicionarioDeCampos['nombreCiudadano']
        flags = Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMaximizeButtonHint
        self.ficha = SincronizaFichaGui(self.iface.mainWindow(), flags)
        self.ficha.setWindowTitle(u'Datos Sincronizados')
        claveCatastral = self.ctrl.comboBox_2.currentText()
        nombrePropietarioData = dicionarioDeCampos['nombreCiudadano']
        self.ficha.claveCatastralData.setText('<i>' + claveCatastral + '</i>')
        self.ficha.nombrePropietarioData.setText('<i>'+nombrePropietarioData + '</i>')
        self.ficha.areaGraficaData.setText('<i>' + "{:.2f}".format(dicionarioDeCampos['area']) + " m<sup>2</sup></b>" + '</i>')
        self.ficha.show()

    def showDialog(self):
        flags = Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMaximizeButtonHint
        self.ctrl = SincronizaGui(self.iface.mainWindow(), flags)
        self.ctrl.setWindowTitle(u'Sincronización')
        self.ctrl.setModal(True)
        self.ctrl.buttonBox.setEnabled(False)
        self.ctrl.comboBox.addItems(self.getCapasCatastroName())
        self.ctrl.show()
        self.ctrl.comboBox.currentIndexChanged.connect(
            lambda: self.getClaves()
        )
        self.ctrl.buttonBox.accepted.connect(
            lambda: self.mainSincroniza()
        )

    def getCapasCatastroName(self):
        """
        Crea una lista de capas que pertenecen al catastro para su sincronizacion con la
        BDD de catastro usando el WS
        :return: Lista de Capas
        """
        layerList = ['-----------------------']
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if layer.type() == QgsMapLayer.VectorLayer:
                typeGeom = QgsWKBTypes.displayString(int(QgsWKBTypes.flatType(int(layer.wkbType()))))
                if layer.name().startswith('m_edit_') and typeGeom == 'MultiPolygon':
                    layerList.append(layer.name())
        return layerList

    def getCapasCatastro(self):
        """
        Crea una lista de capas que pertenecen al catastro para su sincronizacion con la
        BDD de catastro usando el WS
        :return: Lista de Capas
        """
        layerList = []
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if layer.type() == QgsMapLayer.VectorLayer:
                typeGeom = QgsWKBTypes.displayString(int(QgsWKBTypes.flatType(int(layer.wkbType()))))
                if layer.name().startswith(
                        'm_edit_') and layer.type() == QgsMapLayer.VectorLayer and typeGeom == 'MultiPolygon':
                    layerList.append(layer)
        return layerList

    def getCapasCatastroReferencia(self):
        """
        Crea una lista de capas de referencia WFS que pertenecen al catastro en la TOC
        :return: Lista de Capas
        """
        layerList = []
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if layer.type() == QgsMapLayer.VectorLayer:
                typeGeom = QgsWKBTypes.displayString(int(QgsWKBTypes.flatType(int(layer.wkbType()))))
                if layer.name().endswith(
                        'referencia') and layer.type() == QgsMapLayer.VectorLayer and typeGeom == 'MultiPolygon':
                    layerList.append(layer)
        return layerList

    def mainSincroniza(self):
        claveCatastral = self.ctrl.comboBox_2.currentText()
        # datosSincronizacion = self.requestPredioSyncronization(claveCatastral)
        datosSincronizacion = self.wsRequest.requestPredioSyncronization(claveCatastral)
        if self.ctrl.comboBox.currentText() == 'm_edit_predio':
            self.llenaCamposPredio(datosSincronizacion)
            self.FichaSincronizada(datosSincronizacion)
        #TODO: sincronizacion para efificaciones

    def mensaje(self, texto, tipo):
        msg = QMessageBox()
        msg.setIcon(tipo)
        msg.setText(texto)
        msg.setWindowTitle("Sistema de gestion catastral")
        retval = msg.exec_()

    def getClaves(self):
        """
        Obtiene las claves catastrales de la capa de predios m_edit_predio
        :return:
        """
        self.ctrl.comboBox_2.clear()
        selectedLayerIndex = self.ctrl.comboBox.currentIndex()
        layers = self.getCapasCatastro()
        selectedLayer = layers[selectedLayerIndex-1]
        self.ctrl.buttonBox.setEnabled(True)
        if self.ctrl.comboBox.currentText() == 'm_edit_predio':
            idx = selectedLayer.fieldNameIndex(self.campoClavePredio)
        elif self.ctrl.comboBox.currentText() == 'm_edit_edificacion':
            idx = selectedLayer.fieldNameIndex(self.campoClaveEdificacion)
        fields = []
        for feature in selectedLayer.getFeatures():
            fields.append(str(feature.attributes()[idx]))
        self.ctrl.comboBox_2.addItems(fields)

    def llenaCamposPredio(self, dicionarioDeCampos):
        capas = self.getCapasCatastro()
        nombre_propietario = dicionarioDeCampos['nombreCiudadano']
        nombre_predio = dicionarioDeCampos['nombrePredio']
        for capa in capas:
            if capa.name() == 'm_edit_predio':
                features = capa.getFeatures()
                nombre_propietario_idx = capa.fieldNameIndex('nombre_propietario')
                nombre_predio_idx = capa.fieldNameIndex('nombre_predio')
                capa.startEditing()
                for item in features:
                    if item['clave_catastral'] == self.ctrl.comboBox_2.currentText():
                        dicionarioDeCampos['area'] = item.geometry().area()
                        capa.changeAttributeValue(item.id(), nombre_propietario_idx, nombre_propietario)
                        capa.changeAttributeValue(item.id(), nombre_predio_idx, nombre_predio)
                capa.commitChanges()
