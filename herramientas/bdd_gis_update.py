# -*- coding: utf-8 -*-
from bddConexion import BDDConexion
from referencias import Referencia
from qgis.core import QgsMessageLog
from ..utilitarios.configurations import configuracionGlobal


class BDDGisUpdate:
    def __init__(self, iface):
        self.iface = iface
        self.referencias = Referencia(self.iface)
        # 2 tipos de conexiones una para lectura y otra para escritura
        self.conexion = BDDConexion()
        self.conexion_qry = self.conexion.conexion
        self.conexion_qry_editor = self.conexion.conexion_editor
        self.cursor = self.conexion_qry.cursor()
        self.cursor_editor = self.conexion_qry_editor.cursor()

    def updateClaveCatastral(self, bdd_schema, clave_catastral_old, clave_catastral_new):
        """
        Realiza el update en la Base de datos gráfica de la clave catastral del predio y las construcciones
        contenidas en el
        :param bdd_schema: String del esquema corespondiente al municipio
        :param clave_catastral_old: String de la clave catastral anterior del predio grafico
        :param clave_catastral_new:  String de la clave catastral nueva del predio grafico
        :return: Void
        """
        new_manzana = clave_catastral_new[10:13]
        # No sabemos porque en la consultoria original la tabla de edificaciones estan definidos como tipo doble
        # es por esto que se hace la transformacion de claves catastrales a numero
        manzana_number = int(new_manzana)
        sql_update_predio = 'UPDATE ' + bdd_schema + '.predio SET clave_catastral = \'' + \
                     clave_catastral_new + '\' WHERE clave_catastral = \'' + clave_catastral_old + '\''
        self.cursor.execute(sql_update_predio)
        self.conexion_qry.commit()
        sql_update_edificacion = 'UPDATE ' + bdd_schema + '.edificacion SET predio = ' + str(int(clave_catastral_new)) + ', manzana = ' + str(manzana_number) + ', clave_catastral = \'' + clave_catastral_new + '\'|| SUBSTRING(' + bdd_schema + '.edificacion.clave_catastral FROM 20 FOR 23) FROM ' + bdd_schema + '.predio  WHERE predio.clave_catastral = \'' + clave_catastral_new + '\' AND st_within(st_pointonsurface(edificacion.geom), predio.geom);'
        self.cursor.execute(sql_update_edificacion)
        self.conexion_qry.commit()

    def insertAuditoriaCambioClave(self, bdd_schema, clave_catastral_old, clave_catastral_new, user):
        """
        Inserta el cambio a la base de datos grafica en la tabla de auditoria edicion.auditoria_predio
        :param bdd_schema: String del esquema corespondiente al municipio
        :param clave_catastral_old: String de la clave catastral anterior del predio grafico
        :param clave_catastral_new: String de la clave catastral nueva del predio grafico
        :param user:
        :return: Void
        """
        sqlInsertAud = 'INSERT INTO edicion.auditoria_predio (select nextval(\'\"edicion\".\"sc_auditoria_predio\"\'), current_date, \'UPDATE\',\'' + clave_catastral_old + '\' ,\'' + clave_catastral_new + '\' ,\'' + user + '\');'
        self.cursor_editor.execute(sqlInsertAud)
        self.conexion_qry_editor.commit()

