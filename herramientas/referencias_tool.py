# -*- coding: utf-8 -*-
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.core import QgsMessageLog
from qgis.gui import *
import requests

from referencias import Referencia
from ..utilitarios.configurations import configuracionGlobal


class ReferenciaTool:

    def __init__(self, iface, toolbar):
        self.iface = iface
        self.configuracionGlobal = configuracionGlobal()
        self.action_referencia_predio = QAction(QIcon(':/plugins/Catastro2/icons/referencia_predio_icon.png'),
                                         "Referencia Predios",  self.iface.mainWindow())
        self.action_referencia_predio.setEnabled(True)
        self.raizArbolDeCapas = QgsProject.instance().layerTreeRoot()
        self.referenceGroup = None
        self.referenceLayerPredios = None
        self.referenceLayerEdificaciones = None
        toolbar.addAction(self.action_referencia_predio)
        self.action_referencia_predio.triggered.connect(lambda: self.mainReferencias())

        self.referencias = Referencia(self.iface)

    def mainReferencias(self):
        """
        Metodo responsable de cargar las capas de referencia WFS en el canvas de QGIS
        Las capas se cargan si es que no estan previamente cargadas
        :return: Void
        """
        if not self.referencias.layerExists('Predio referencia'):
            self.referencias.addLayer(self.referencias.getWfsUri('predio'), 'Predio referencia', 'predio_reference.qml')
        if not self.referencias.layerExists('Edificacion referencia'):
            self.referencias.addLayer(self.referencias.getWfsUri('edificacion'), 'Edificacion referencia', 'edificacion_reference.qml')









