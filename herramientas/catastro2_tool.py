# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import QgsMessageLog
from qgis.core import QgsDataSourceURI, QgsVectorLayer, QgsMapLayerRegistry, QgsMapLayer, QgsExpression, QgsFeatureRequest
import ConfigParser
import hmac
import os.path
import psycopg2
import psycopg2.extras
from catastro2_tool_gui import Catastro2Gui
from catastro_2 import Catastro2
from referencias import Referencia
from sincroniza_tool import SincronizaTool
from referencias_tool import ReferenciaTool
from cambio_de_clave_tool import CambioClaveTool
from ..utilitarios.configurations import configuracionGlobal
from ..sesion.catastroSesion import SesionCatastro


class Catastro2Tool:
    barraDeHerramientasCargada = True
    def __init__(self, iface, toolbar):
        self.iface = iface
        self.toolbar = toolbar
        self.action_orden_de_trabajo = QAction(QIcon(':/plugins/Catastro2/icons/catastro_icon.png'),
                                               u"Gestión de Ordenes de Trabajo", self.iface.mainWindow())
        toolbar.addAction(self.action_orden_de_trabajo)
        self.action_orden_de_trabajo.triggered.connect(self.showDialog)
        flags = Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMaximizeButtonHint
        self.catastroDialog = Catastro2Gui(self.iface.mainWindow(), flags)
        self.configuracionGlobal = configuracionGlobal()
        self.iface.mainWindow().statusBar().showMessage(u'Catastro AME')

        # Connexion a la base de datos
        self.conn = None
        self.cur = None
        self.sesion = False
        self.catastroSesion = SesionCatastro()
        self.referencias = Referencia(self.iface)
        self.Catastro2 = Catastro2(self)

    def showDialog(self):
        if self.catastroDialog:
            self.catastroDialog.setWindowTitle(u'Catastro Ordenes de Trabajo')
            if not self.sesion:
                self.catastroDialog.initGui()
            self.catastroDialog.show()
            self.catastroDialog.iniciarSesionButton.clicked.connect(lambda: self.autenticar())
            self.catastroDialog.terminarEdicionButton.clicked.connect(lambda: self.paso_produccion())
            self.catastroDialog.guardarEditarButton.clicked.connect(lambda: self.reanudar_edicion())
        else:
            self.mensaje(u'El usuario ya tiene abierta la bandeja de trámites')

    def iniciarSesion(self):
        strConexion = self.configuracionGlobal.conexion
        usuario = self.catastroDialog.userInputLine.text()
        self.catastroSesion.usuario = usuario
        self.Catastro2.setCatastroUser(usuario)
        password = self.catastroDialog.userPwdLine.text()
        if str(usuario) and str(password):
            hash = hmac.new(str(usuario), str(password)).hexdigest()
        try:
            self.conn = psycopg2.connect(strConexion)
            self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            try:
                self.cur.execute(
                    "SELECT a.*,b.id_mpio  from mapgis.usuario a, catgis.editores b where clave ='" + hash + "' and identificador = '" + str(
                        usuario) + "' and a.id_usuario = b.id_usuario")
                resultado = ''
                resultado_idm = ''
                rows = self.cur.fetchall()
                for row in rows:
                    resultado = str(row['id_usuario'])
                    resultado_idm = str(row['id_mpio'])
                if resultado == '':
                    self.mensaje(u'Usuario o Contraseña, incorrectos.')
                else:
                    self.catastroDialog.tableWidget.setEnabled(True)
                    self.catastroDialog.buscarButton.setEnabled(True)
                    self.catastroDialog.buscarLine.setEnabled(True)
                    self.catastroDialog.userInputLine.setEnabled(False)
                    self.catastroDialog.userPwdLine.setEnabled(False)
                    self.catastroDialog.iniciarSesionButton.setText(u'Cerrar sesion')
                    self.catastroDialog.label_3.setText('1')
                    self.catastroSesion.activarSesion()
                    # Carga de herramientas de gestion catastral luego de iniciar sesion
                    if Catastro2Tool.barraDeHerramientasCargada:
                        self.sincroniza_tool = SincronizaTool(self.iface, self.toolbar)
                        self.referencias_tool = ReferenciaTool(self.iface, self.toolbar)
                        self.cambio_clave_tool = CambioClaveTool(self.iface, self.toolbar)
                        Catastro2Tool.barraDeHerramientasCargada = False

                    ##CARGAR GRID
                    self.catastroDialog.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
                    self.catastroDialog.tableWidget.setColumnCount(6)
                    self.catastroDialog.tableWidget.setHorizontalHeaderLabels(
                        ['Orden trabajo', 'Tramite ID', 'Descripcion', 'Fecha', 'Estado Orden', 'Editar'])
                    self.catastroDialog.tableWidget.setSortingEnabled(True)
                    try:
                        self.conn = psycopg2.connect(strConexion)
                        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
                        try:
                            self.cur.execute(
                                "SELECT distinct a.id_ot, a.codigo, a.descripcion, a.fecha ,b.nombre as responsable ,catgis.dom_estado_ot.descripcion as estadoot, a.tipo_edicion  from  catgis.orden_trabajo a,  catgis.dom_tipo_ot,  catgis.estado_ot ot,  catgis.dom_estado_ot, mapgis.usuario b, catgis.editores, catgis.municipio, (select catgis.estado_ot.fk_id_ot orden, max(id_estado_ot) estado from catgis.estado_ot group by catgis.estado_ot.fk_id_ot)Est  where  a.id_ot = ot.fk_id_ot  and catgis.dom_estado_ot.id = ot.estado  and (Est.orden=a.id_ot and Est.estado=ot.id_estado_ot ) and a.fk_mpio = catgis.municipio.cod_mpio  and b.id_usuario=catgis.editores.id_usuario  and a.fk_mpio='" + resultado_idm + "' and catgis.editores.id_usuario=" + resultado + " and ot.estado in (2,3)")
                            resultado = ''
                            rows = self.cur.fetchall()
                            self.catastroDialog.tableWidget.setRowCount(0)
                            self.catastroDialog.tableWidget.setRowCount(len(rows) + 1)
                            cont = 0
                            for row in rows:
                                id_ot = QTableWidgetItem(str(row["id_ot"]))
                                codigo = QTableWidgetItem(row["codigo"])
                                descripcion = QTableWidgetItem(row["descripcion"])
                                fecha = QTableWidgetItem(str(row["fecha"]))
                                estadoOrden = QTableWidgetItem(row["estadoot"])
                                self.catastroDialog.tableWidget.setItem(cont, 0, id_ot)
                                self.catastroDialog.tableWidget.setItem(cont, 1, codigo)
                                self.catastroDialog.tableWidget.setItem(cont, 2, descripcion)
                                self.catastroDialog.tableWidget.setItem(cont, 3, fecha)
                                self.catastroDialog.tableWidget.setItem(cont, 4, estadoOrden)
                                boton = QPushButton()
                                boton.setText('Editar')
                                boton.setFixedWidth(50)
                                boton.clicked.connect(lambda: self.cargar_capa_boton())
                                self.catastroDialog.tableWidget.setCellWidget(cont, 5, boton)
                                cont = cont + 1
                        except:
                            self.mensaje(u"Error al consultar")
                    except:
                        self.mensaje(u"No es posible conectarse a la base de datos")
                        self.closeDBConnexion()
            except:
                self.mensaje(u"Usuario o clave no válidos")
                self.closeDBConnexion()
        except:
            self.mensaje(u"No es posible conectarse a la base de datos")
            self.closeDBConnexion()

    def autenticar(self):
        estadoSesion = int(self.catastroDialog.label_3.text())
        if (estadoSesion == 0):
            self.iniciarSesion()
            self.sesion = True
        else:
            self.cerrarSesion()
            self.sesion = False

    def closeDBConnexion(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None

    def cambiar_estado(self, estado, fk_id_ot):
        strConexion = self.configuracionGlobal.conexion
        try:
            self.conn = psycopg2.connect(strConexion)
            self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

            sqlObs = "select observacion from catgis.estado_ot where fk_id_ot = " + fk_id_ot + "  limit 1"
            sqlUsuario = "select responsable from catgis.estado_ot where fk_id_ot = " + fk_id_ot + "  limit 1"
            if (estado == '4'):
                sqlObs = "'Edicion desde QGIS'"
                sqlUsuario = "select a.responsable from catgis.municipio a, catgis.orden_trabajo b where b.fk_mpio = a.cod_mpio and b.id_ot = " + fk_id_ot

            try:
                self.cur.execute(
                    "INSERT INTO catgis.estado_ot(estado, responsable, fecha, observacion, fk_id_ot) VALUES (" + estado + ", (" + sqlUsuario + "), CURRENT_TIMESTAMP, (" + sqlObs + "), " + fk_id_ot + ")")
                self.conn.commit()
                self.closeDBConnexion()
            except:
                self.mensaje("Error al ejecutar la consulta")
        except:
            self.mensaje("No es posible conectarse a la base de datos")

    def cerrarSesion(self):
        self.remover_capa('m_edit_predio')
        self.remover_capa('m_edit_edificacion')
        self.remover_capa('Edificacion referencia')
        self.remover_capa('Predio referencia')
        self.catastroDialog.tableWidget.setEnabled(False)
        self.catastroDialog.tableWidget.clearContents()
        self.catastroDialog.userInputLine.setText('')
        self.catastroDialog.userPwdLine.setText('')
        self.catastroDialog.userInputLine.setEnabled(True)
        self.catastroDialog.userPwdLine.setEnabled(True)
        self.catastroDialog.iniciarSesionButton.setText('Iniciar sesion')
        self.catastroDialog.label_5.setText("Orden de trabajo: ")
        self.closeDBConnexion()
        # Remuevo las herramientas creadas en el inicio de sesion
        for toolbar in self.iface.mainWindow().findChildren(QToolBar):
            if toolbar.objectName() == 'Catastro2':
                actions = toolbar.actions()
                for action in actions:
                    # Remuevo todas exepto la de ordenes de trabajo
                    if action.text() != u'Gestión de Ordenes de Trabajo':
                        action.setVisible(False)
                        toolbar.removeAction(action)
                        del action
        self.sesion = False
        self.catastroDialog.close()
        self.catastroDialog.destroy()
        self.cambio_clave_tool.limpiaFicha()
        self.cambio_clave_tool.close()
        flags = Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMaximizeButtonHint
        self.catastroDialog = Catastro2Gui(self.iface.mainWindow(), flags)
        Catastro2Tool.barraDeHerramientasCargada = True

    def mensaje(self, texto):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(texto)
        msg.setWindowTitle("Sistema de gestion catastral")
        retval = msg.exec_()

    def cargar_capa_boton(self):
        button = self.catastroDialog.sender()
        fk_id_ot = self.catastroDialog.tableWidget.itemAt(10, button.pos().y())
        self.catastroDialog.label_4.setText(fk_id_ot.text().strip())
        self.catastroDialog.label_5.setText("Orden de trabajo: " + fk_id_ot.text().strip())
        item = self.catastroDialog.tableWidget.itemAt(100, button.pos().y())
        self.remover_capa("m_edit_predio")
        self.remover_capa("m_edit_edificacion")
        if not self.referencias.layerExists('Predio referencia'):
            self.referencias.addLayer(self.referencias.getWfsUri('predio'), 'Predio referencia', 'predio_reference.qml')
        if not self.referencias.layerExists('Edificacion referencia'):
            self.referencias.addLayer(self.referencias.getWfsUri('edificacion'), 'Edificacion referencia', 'edificacion_reference.qml')
        if item is not None:
            # archivo ini
            settings = ConfigParser.ConfigParser()

            dirname, filename = os.path.split(os.path.abspath(__file__))
            servidor = self.configuracionGlobal.servidor
            puerto = self.configuracionGlobal.puerto
            base_datos = self.configuracionGlobal.dbname
            usuario = self.configuracionGlobal.enc_usuario_edita_capa
            #usuario = self.configuracionGlobal.enc_usuario
            clave = self.configuracionGlobal.enc_clave_edita_capa
            #clave = self.configuracionGlobal.enc_clave
            uri = QgsDataSourceURI()
            uri.setConnection(servidor, puerto, base_datos, usuario, clave)
            uri.setDataSource("edicion", "predio", "geom", "")
            vlayer = QgsVectorLayer(uri.uri(True), "m_edit_predio", usuario)
            vlayer.loadNamedStyle(dirname + os.sep + 'm_edit_predio.qml')
            vlayer.triggerRepaint()
            QgsMapLayerRegistry.instance().addMapLayer(vlayer)

            # cargar capa
            if not vlayer.isValid():
                QgsMessageLog.logMessage(servidor+ puerto+ base_datos+ usuario+ clave,"1:Layer failed to load!")
            canvas = self.iface.mapCanvas()
            name = "m_edit_predio"
            layer = QgsMapLayerRegistry.instance().mapLayersByName(name)[0]
            # filtrado
            layer.setSubsetString("id_ot = " + fk_id_ot.text().strip())
            layer.removeSelection()
            expr = QgsExpression("id_ot = " + fk_id_ot.text().strip())
            it = layer.getFeatures(QgsFeatureRequest(expr))
            ids = [i.id() for i in it]
            layer.setSelectedFeatures(ids)
            canvas.zoomToSelected(layer)
            layer.removeSelection()
            uri = QgsDataSourceURI()
            uri.setConnection(servidor, puerto, base_datos, usuario, clave)
            uri.setDataSource("edicion", "edificacion", "geom", "")
            vlayer = QgsVectorLayer(uri.uri(True), "m_edit_edificacion", usuario)
            vlayer.loadNamedStyle(dirname + os.sep + 'm_edit_edificacion.qml')
            vlayer.triggerRepaint()
            QgsMapLayerRegistry.instance().addMapLayer(vlayer)
            # cargar capa
            if not vlayer.isValid():
                QgsMessageLog.logMessage(servidor + puerto + base_datos + usuario + clave, "2:Layer failed to load!")
            canvas = self.iface.mapCanvas()
            name = "m_edit_edificacion"
            layer = QgsMapLayerRegistry.instance().mapLayersByName(name)[0]
            # filtrado
            layer.setSubsetString("id_ot = " + fk_id_ot.text().strip())
            canvas.refresh()
            self.cambiar_estado('3', fk_id_ot.text().strip())
            self.catastroDialog.guardarEditarButton.setEnabled(True)
            self.catastroDialog.terminarEdicionButton.setEnabled(True)
            self.catastroDialog.cancelarRechazarButton.setEnabled(True)
            self.catastroDialog.tableWidget.setEnabled(False)
            self.catastroDialog.buscarLine.setEnabled(False)
            self.catastroDialog.buscarButton.setEnabled(False)

    def remover_capa(self, capa):
        layers = self.iface.legendInterface().layers()
        for layer in layers:
            if capa == str(layer.name()):
                QgsMapLayerRegistry.instance().removeMapLayer(layer.id())

    def paso_produccion(self):
        texto = u'¿Desea terminar la edicion actual y pasarla para revisión?'
        reply = QMessageBox.question(self.iface.mainWindow(), 'Sistema de gestion catastral', texto, QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.cambiar_estado('4', self.catastroDialog.label_4.text())
            self.reanudar_edicion()
            self.iniciarSesion()
            self.catastroDialog.label_5.setText("Orden de trabajo: ")
        else:
            print "no"

    def reanudar_edicion(self):
        self.remover_capa("m_edit_predio")
        self.remover_capa("m_edit_edificacion")
        self.catastroDialog.tableWidget.setEnabled(True)
        self.catastroDialog.guardarEditarButton.setEnabled(False)
        self.catastroDialog.terminarEdicionButton.setEnabled(False)
        self.catastroDialog.label_5.setText("Orden de trabajo: ")
