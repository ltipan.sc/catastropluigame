# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from sincroniza_dialog_ficha import Ui_FichaSincronizada
import os, sys


class SincronizaFichaGui(QDialog, Ui_FichaSincronizada):
    def __init__(self, parent, flags):
        QDialog.__init__(self, parent, flags)
        self.setupUi(self)
        self.method = "fixed"
        self.claveCatastralData = self.claveCatastralData
        self.nombrePropietarioData = self.nombrePropietarioData
        self.areaGraficaData = self.areaGraficaData

    def initGui(self):
        pass