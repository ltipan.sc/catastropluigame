
from PyQt4.QtCore import Qt
from qgis.gui import QgsMapTool
from qgis.core import QgsMessageLog, QgsPoint, QgsVectorLayer
from PyQt4.QtCore import *
from qgis.core import *
from qgis.gui import *
from qgis.utils import iface



class SendPointToolCoordinates(QgsMapTool):
    """
    Habilita la seleccion de una geometria en una capa especifica
    """
    def __init__(self, canvas, layer, widget):
        QgsMapTool.__init__(self, canvas)
        self.widget = widget
        self.canvas = canvas
        self.layer = layer
        self.predioSelected = None
        self.pointLayer = None
        self.setCursor(Qt.CrossCursor)

    def canvasReleaseEvent(self, event):
        """
        Metodo que se ejecuta la momento de soltar el boton izquierdo del raton sobre el canvas de layers
        :param event: Evento Clic para seleccionar el predio en pantalla (Canvas)
        :return: Void
        """
        self.layer.removeSelection()
        self.canvas.refresh()
        point_event = self.toLayerCoordinates(self.layer, event.pos())
        self.pointLayer = QgsVectorLayer('Point?crs=epsg:32717', 'point', 'memory')
        # Set the provider to accept the data source
        prov = self.pointLayer.dataProvider()
        point = QgsPoint(point_event.x(), point_event.y())
        # Add a new feature and assign the geometry
        featPoint = QgsFeature()
        featPoint.setGeometry(QgsGeometry.fromPoint(point))
        prov.addFeatures([featPoint])
        self.pointLayer.updateExtents()
        # Se debe optimizar el codigo para el uso de indices espaciales
        # en la presente version esto no se usa
        indexPoint = QgsSpatialIndex()  # Spatial index
        indexPolygon = QgsSpatialIndex()  # Spatial index
        for ft in self.pointLayer.getFeatures():
            indexPoint.insertFeature(ft)
        for ft in self.layer.getFeatures():
            indexPolygon.insertFeature(ft)
        selection = []
        for polygon in self.layer.getFeatures():
            for point in self.pointLayer.getFeatures():
                if polygon.geometry().intersects(point.geometry()):
                    self.layer.setSelectedFeatures([polygon.id()])
        del selection[:]
        self.predioSelected = self.layer.selectedFeatures()[0]
        claveCatastralAnterior = self.predioSelected[1]
        self.widget.originalClaveCatastral.setText(claveCatastralAnterior)


