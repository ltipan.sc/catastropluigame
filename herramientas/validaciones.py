import re
from bddConexion import BDDConexion
from ..utilitarios.configurations import configuracionGlobal
from qgis.core import QgsMessageLog


class Validaciones:

    def __init__(self):
        self.configuracionGlobal = configuracionGlobal()
        self.conexion = BDDConexion()
        self.conexion_qry = self.conexion.conexion
        self.cursor = self.conexion.cursor
        self.MatchClaveCatastral = None

    def validaClaveCatastral(self, claveCatastral):
        """
        Validacion mediante una expresion regular la validez de la clave catastral ingresada por el usuario
        :param claveCatastral: string
        :return: boolean
        """
        # Expresion regular solo admite numeros longitud 19
        validacion = re.match('^[0-9]{19}$', claveCatastral)
        if validacion:
            return True
        else:
            return False

    def validaClaveDuplicada(self, claveCatastral):
        buscaClaveDuplicadaEnBDD = 'SELECT predio FROM ' + self.configuracionGlobal.dbSchema + '.predio WHERE clave_catastral = \'' + claveCatastral + '\''
        self.cursor.execute(buscaClaveDuplicadaEnBDD)
        claves = self.cursor.fetchall()
        if len(claves) == 0:
            return True
        else:
            return False

