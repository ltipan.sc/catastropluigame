# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Catastro2
                                 A QGIS plugin
 Catastro 2.0
                              -------------------
        begin                : 2018-11-23
        git sha              : $Format:%H$
        copyright            : (C) 2018 by AME
        email                : jorge.cabrera@ame.gob.ec
 ***************************************************************************/
"""
import sys
import os.path
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.core import QgsMessageLog

# Initialize Qt resources from file resources.py
import resources

# Import the code for the dialog
from catastro_2_dialog import Catastro2Dialog
from catastro2_about_gui import CatastroAboutGui
from herramientas.catastro2_tool import Catastro2Tool
reload(sys)
sys.setdefaultencoding('utf-8')



class Catastro2:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'Catastro2_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)


        # Declare instance attributes
        self.actions = []

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('Catastro2', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = Catastro2Dialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action


    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        icon_path = QIcon(':/plugins/Catastro2/icons/icon.png')
        self.toolbar = self.iface.addToolBar("Catastro2")
        self.toolbar.setObjectName("Catastro2")
        # Creacion del Menu Catastro AME
        self.menu = QMenu()
        self.menu.setTitle("Catastro AME")
        self.catastro_help = QAction(icon_path, QCoreApplication.translate("Catastro2", "Ayuda"),
                                     self.iface.mainWindow())
        self.catastro_about = QAction(QIcon(':/plugins/Catastro2/icons/about_icon.png'), QCoreApplication.translate("Catastro2", "Acerca de"),
                                      self.iface.mainWindow())
        self.catastro_settings = QAction(icon_path, QCoreApplication.translate("Catastro2", "Configuracion"),
                                         self.iface.mainWindow())
        # self.menu.addActions([self.catastro_help, self.catastro_about, self.catastro_settings])
        self.menu.addActions([self.catastro_about])
        menu_bar = self.iface.mainWindow().menuBar()
        self.actions = menu_bar.actions()
        lastAction = self.actions[len(self.actions) - 1]
        menu_bar.insertMenu(lastAction, self.menu)

        self.catastro_about.triggered.connect(self.doAbout)
        # Inserta las herramientas en la barra de herramientas
        self.catastro = Catastro2Tool(self.iface, self.toolbar)

    def unload(self):
        self.catastro.catastroDialog.close()
        self.catastro.catastroDialog.destroy()
        self.catastro.cambio_clave_tool.close()
        for toolbar in self.iface.mainWindow().findChildren(QToolBar):
            if toolbar.objectName() == 'Catastro2':
                Rtoolbar = toolbar
                actions = toolbar.actions()
                for action in actions:
                    # Remuevo todas exepto la de ordenes de trabajo
                    Rtoolbar.removeAction(action)
                    del action
                    Rtoolbar.close()
                    Rtoolbar.destroy()
        del Rtoolbar
        del self.toolbar
        del self.menu

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass

    def doAbout(self):
        d = CatastroAboutGui(self.iface.mainWindow())
        d.show()