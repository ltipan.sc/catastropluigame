# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'catastro2_about.ui'
#
# Created: Thu Nov 29 12:01:20 2018
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CatastroAMEAbout(object):
    def setupUi(self, CatastroAMEAbout):
        CatastroAMEAbout.setObjectName(_fromUtf8("CatastroAMEAbout"))
        CatastroAMEAbout.resize(305, 253)
        self.gridlayout = QtGui.QGridLayout(CatastroAMEAbout)
        self.gridlayout.setObjectName(_fromUtf8("gridlayout"))
        self.btnClose = QtGui.QPushButton(CatastroAMEAbout)
        self.btnClose.setObjectName(_fromUtf8("btnClose"))
        self.gridlayout.addWidget(self.btnClose, 6, 3, 1, 1)
        self.lblTitle = QtGui.QLabel(CatastroAMEAbout)
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.lblTitle.setFont(font)
        self.lblTitle.setTextFormat(QtCore.Qt.RichText)
        self.lblTitle.setObjectName(_fromUtf8("lblTitle"))
        self.gridlayout.addWidget(self.lblTitle, 0, 0, 1, 3)
        self.txtAbout = QtGui.QTextEdit(CatastroAMEAbout)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        self.txtAbout.setPalette(palette)
        self.txtAbout.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.txtAbout.setAutoFillBackground(True)
        self.txtAbout.setFrameShape(QtGui.QFrame.NoFrame)
        self.txtAbout.setFrameShadow(QtGui.QFrame.Plain)
        self.txtAbout.setReadOnly(True)
        self.txtAbout.setObjectName(_fromUtf8("txtAbout"))
        self.gridlayout.addWidget(self.txtAbout, 3, 0, 1, 4)
        self.lblVersion = QtGui.QLabel(CatastroAMEAbout)
        self.lblVersion.setObjectName(_fromUtf8("lblVersion"))
        self.gridlayout.addWidget(self.lblVersion, 1, 0, 1, 3)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridlayout.addItem(spacerItem, 5, 2, 1, 1)
        self.btnWeb = QtGui.QPushButton(CatastroAMEAbout)
        self.btnWeb.setObjectName(_fromUtf8("btnWeb"))
        self.gridlayout.addWidget(self.btnWeb, 6, 2, 1, 1)
        self.label = QtGui.QLabel(CatastroAMEAbout)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridlayout.addWidget(self.label, 2, 0, 1, 1)
        self.btnHelp = QtGui.QPushButton(CatastroAMEAbout)
        self.btnHelp.setObjectName(_fromUtf8("btnHelp"))
        self.gridlayout.addWidget(self.btnHelp, 6, 1, 1, 1)

        self.retranslateUi(CatastroAMEAbout)
        QtCore.QObject.connect(self.btnClose, QtCore.SIGNAL(_fromUtf8("clicked()")), CatastroAMEAbout.reject)
        QtCore.QMetaObject.connectSlotsByName(CatastroAMEAbout)

    def retranslateUi(self, CatastroAMEAbout):
        CatastroAMEAbout.setWindowTitle(_translate("CatastroAMEAbout", "Acerca de", None))
        self.btnClose.setText(_translate("CatastroAMEAbout", "Cerrar", None))
        self.lblTitle.setText(_translate("CatastroAMEAbout", "<h2>Catastro AME</h2>", None))
        self.txtAbout.setHtml(_translate("CatastroAMEAbout", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Sans Serif\'; font-size:9pt;\"><br /></p></body></html>", None))
        self.lblVersion.setText(_translate("CatastroAMEAbout", "Version x.x-xxxxxx", None))
        self.btnWeb.setText(_translate("CatastroAMEAbout", "Web", None))
        self.label.setText(_translate("CatastroAMEAbout", "<html><head/><body><p><img src=\":/plugins/Catastro2/icons/logoPlataforma.png\"/></p></body></html>", None))
        self.btnHelp.setText(_translate("CatastroAMEAbout", "Ayuda", None))