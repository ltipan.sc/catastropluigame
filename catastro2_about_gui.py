# -*- coding: latin1 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from catastro2_about import Ui_CatastroAMEAbout
import webbrowser, os

currentPath = os.path.dirname(__file__)


class CatastroAboutGui(QDialog, QObject, Ui_CatastroAMEAbout):
    def __init__(self, iface):
        QDialog.__init__(self, iface)
        self.iface = iface
        self.setupUi(self)
        self.btnWeb.clicked.connect(self.openWeb)
        self.btnHelp.clicked.connect(self.openHelp)
        self.lblVersion.setText("V2.0")
        self.txtAbout.setText(self.getText())

    def openWeb(self):
        webbrowser.open("https://plataformamunicipal.ame.gob.ec/")

    def openHelp(self):
        webbrowser.open("https://plataformamunicipal.ame.gob.ec/")

    def getText(self):
        return self.tr('Catastro AME es un plugin desarrollado por la Plataforma Tecnológica Municipal' '\nSu objetivo '
                       'es brindar una herramienta de enlace entre la cartografía digital y el catastro gestionado '
                       'en SIC - AME')

