# -*- coding: utf-8 -*-
import ConfigParser
import os
from qgis.core import QgsMessageLog
import requests
import xml.etree.cElementTree as ET
from qgis.core import QgsMessageLog


class configuracionGlobal:

    def __init__(self):
        self.conexion = self.setConexion(self.IniFileConnexion())
        self.conexion_editor = self.setConexionEditor(self.IniFileConnexion())
        self.ruc = self.setRuc(self.IniFileConnexion())
        self.sincronizacionEndPoint = self.setSincronizacionEndPoint(self.IniFileConnexion())
        self.geoserverIp = self.setGeoserverIp(self.IniFileConnexion())
        self.geoserverPort = self.setGeoserverPort(self.IniFileConnexion())
        self.srsCapas = self.setSrsCapas(self.IniFileConnexion())
        self.geoserverWorkspace = self.setGeoserverWorkspace(self.IniFileConnexion())
        self.dbname = self.setdbName(self.IniFileConnexion())
        self.dbSchema = self.setDbSchema(self.IniFileConnexion())
        self.puerto = self.setPuerto(self.IniFileConnexion())
        self.servidor = self.setServidor(self.IniFileConnexion())
        self.enc_usuario = self.setUsuarioEnc(self.IniFileConnexion())
        self.enc_clave = self.setClaveEnc(self.IniFileConnexion())
        self.enc_usuario_edita_capa = self.setUsuarioEditaCapaEnc(self.IniFileConnexion())
        self.enc_clave_edita_capa = self.setClaveEditaCapaEnc(self.IniFileConnexion())

    def IniFileConnexion(self):
        """
        Crea la conexión al archivo ini de configuración
        Da acceso a las variables de entorno en el archivo ini en el plugin
        :return: Conexion al archivo ini de configuracion
        """
        settings = ConfigParser.ConfigParser()
        dirname, filename = os.path.split(os.path.abspath(__file__))
        settings.read(dirname + os.sep + 'config.ini')
        return settings

    def setConexionEditor(self, conexionArchivoIni):
        """
        Conforma el string que se usa para la conexión a la base de datos para edicion
        :param conexionArchivoIni: Objeto de conexión creado por el metodo IniFileConnexion
        :return: String de conexion de editor para BDD
        """
        endpoint = conexionArchivoIni.get('WS', 'syncPredios')
        servidor = conexionArchivoIni.get('conexion', 'servidor')
        puerto = conexionArchivoIni.get('conexion', 'puerto')
        base_datos = conexionArchivoIni.get('conexion', 'base_datos')
        usuario = conexionArchivoIni.get('conexion', 'enc_usuario_editor')
        usuario_decripted = self.requestUserPwd(usuario, endpoint)
        clave = conexionArchivoIni.get('conexion', 'enc_clave_editor')
        clave_decrypted = self.requestUserPwd(clave, endpoint)
        strConexion = "dbname='" + base_datos + "' user='" + usuario_decripted[
            'return'] + "' host='" + servidor + "' password='" + clave_decrypted['return'] + "'" + " port='" + puerto + "'"
        return strConexion

    def setConexion(self, conexionArchivoIni):
        """
        Conforma el string que se usa para la conexión a la base de datos
        :param conexionArchivoIni: Objeto de conexión creado por el metodo IniFileConnexion
        :return: String de la conexion a la BDD
        """
        endpoint = conexionArchivoIni.get('WS', 'syncPredios')
        servidor = conexionArchivoIni.get('conexion', 'servidor')
        puerto = conexionArchivoIni.get('conexion', 'puerto')
        base_datos = conexionArchivoIni.get('conexion', 'base_datos')
        usuario = conexionArchivoIni.get('conexion', 'enc_usuario')
        usuario_decripted = self.requestUserPwd(usuario,endpoint)
        clave = conexionArchivoIni.get('conexion', 'enc_clave')
        clave_decrypted = self.requestUserPwd(clave,endpoint)
        strConexion = "dbname='" + base_datos + "' user='" + usuario_decripted[
            'return'] + "' host='" + servidor + "' password='" + clave_decrypted['return'] + "'" + " port='" + puerto + "'"
        return strConexion

    def setUsuarioEnc(self, conexionArchivoIni):
        endpoint = conexionArchivoIni.get('WS', 'syncPredios')
        enc_usuario = conexionArchivoIni.get('conexion', 'enc_usuario')
        usuario_decrypted = self.requestUserPwd(enc_usuario,endpoint)
        return usuario_decrypted['return']

    def setClaveEnc(self, conexionArchivoIni):
        endpoint = conexionArchivoIni.get('WS', 'syncPredios')
        enc_clave = conexionArchivoIni.get('conexion', 'enc_clave')
        clave_decrypted = self.requestUserPwd(enc_clave,endpoint)
        return clave_decrypted['return']

    def setUsuarioEditaCapaEnc(self, conexionArchivoIni):
        endpoint = conexionArchivoIni.get('WS', 'syncPredios')
        enc_usuario_editor = conexionArchivoIni.get('conexion', 'enc_usuario_editor')
        usuario_editor_decrypted = self.requestUserPwd(enc_usuario_editor,endpoint)
        return usuario_editor_decrypted['return']

    def setClaveEditaCapaEnc(self, conexionArchivoIni):
        endpoint = conexionArchivoIni.get('WS', 'syncPredios')
        enc_clave_editor = conexionArchivoIni.get('conexion', 'enc_clave_editor')
        clave_editor_decrypted = self.requestUserPwd(enc_clave_editor,endpoint)
        return clave_editor_decrypted['return']

    def setDbSchema(self, conexionArchivoIni):

        schema = conexionArchivoIni.get('bdd_schemma', 'schema')
        return schema

    def setdbName(self, conexionArchivoIni):

        dbName = conexionArchivoIni.get('conexion', 'base_datos')
        return dbName

    def setPuerto(self, conexionArchivoIni):

        puerto = conexionArchivoIni.get('conexion', 'puerto')
        return puerto

    def setServidor(self, conexionArchivoIni):

        servidor = conexionArchivoIni.get('conexion', 'servidor')
        return servidor

    def setRuc(self, conexionArchivoIni):
        ruc = conexionArchivoIni.get('gadmRuc', 'ruc')
        return ruc

    def setSincronizacionEndPoint(self, conexionArchivoIni):
        sincronizacionEndPoint = conexionArchivoIni.get('WS', 'syncPredios')
        return sincronizacionEndPoint

    def setGeoserverIp(self, conexionArchivoIni):
        geoserverIp = conexionArchivoIni.get('geoserver','servidorIp')
        return geoserverIp

    def setGeoserverPort(self,conexionArchivoIni):
        geoserverPort = conexionArchivoIni.get('geoserver', 'servidorPuerto')
        return geoserverPort

    def setSrsCapas(self,conexionArchivoIni):
        srsCapas = conexionArchivoIni.get('srs', 'srs')
        return srsCapas

    def setGeoserverWorkspace(self, conexionArhivoIni):
        workspace = conexionArhivoIni.get('geoserver', 'geoserverWorkspace')
        return workspace

    def requestUserPwd(self, encryptedKey, syncEndpoint):
        with requests.Session() as sesionSync:
            sesionSync.get(syncEndpoint)
            sesionSync.auth = ('ame2017', 'ame2017')
            xmlForRequest = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice/">
                                      <soapenv:Header/>
                                      <soapenv:Body>
                                         <web:desencriptar>
                                            <!--Optional:-->
                                            <texto>""" + encryptedKey + """"</texto>
                                         </web:desencriptar>
                                      </soapenv:Body>
                                   </soapenv:Envelope>"""
            headers = {'Content-Type': 'application/xml'}
            response = ET.fromstring(
                sesionSync.post(syncEndpoint, data=xmlForRequest, headers=headers).text.decode('utf-8'))
            # Crea diccionario para generar respuesta de la funcion
            responseDictionary = {}
            for child in response:
                for element in child:
                    for e in element:
                        responseDictionary[e.tag] = e.text
            return responseDictionary



